FROM maven:3.9.2-eclipse-temurin-11-alpine
WORKDIR /app
COPY . .
RUN mvn package -DskipTests

FROM openjdk:11-jre-slim-buster
COPY --from=0 /app/target/*.jar ./app.jar
ENTRYPOINT java -jar app.jar
